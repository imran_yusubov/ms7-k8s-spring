package az.ingress.ms7k8s;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms7K8sApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms7K8sApplication.class, args);
	}

}
